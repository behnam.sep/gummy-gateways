using System;
using System.Collections;
using DG.Tweening;
using Gameplay.PowerUps;
using Newtonsoft.Json;
using UnityEngine;

namespace Tutorial
{
    [Serializable]
    public class PositivePowerUpsTutorial : BaseTutorial
    {

        #region SERIALIZED PRIVATE FIELDS

        [JsonIgnore] public SlotPowerUpsControl powerUpForThisTutorial;
        [JsonIgnore] public float powerUpValue = 2;
        [JsonIgnore] public Transform tutorialUIObject;
        [JsonIgnore] public Transform handFirstPos;
        [JsonIgnore] public Transform handSecondPos;

        #endregion


        #region PRIVATE METHODS
        
        private Tween _handAnimation;
        
        #endregion
        
        #region PUBLIC METHODS

        public override void MakeEventSubscriptions()
        {

            if (isTutorialShowed)
                return;
            
            
            _tutorialManager.PowerUpsSpawner.OnPowerUpSelection += (count) =>
            {
                if (count < 4 && !isTutorialShowed)
                {
                    _tutorialManager.PowerUpsSpawner.CurrentSelectedPrefab = powerUpForThisTutorial;
                }
            };

            _tutorialManager.PowerUpsSpawner.OnPowerUpSpawn += (powerup,count) =>
            {
                if (count < 4 &&  !isTutorialShowed)
                {
                    powerup.GetComponent<TransactionPowerUps>().SetValueAndUI(2);
                }

                if (count == 3 && !isTutorialShowed)
                    _tutorialManager.StartCoroutine(StartUITutorial());

            };
            
        }
        
        #endregion
        
        #region PRIVATE METHODS

        private IEnumerator StartUITutorial()
        {
            if(isTutorialShowed || _tutorialManager.IsTutorialsDisabled) yield break;

            
            
            yield return new WaitForSeconds(tutorialDelay);
            
            _tutorialManager.TutorialMainText.text = "";
            // Button settings
            var button = _tutorialManager.TutorialButton;
            button.gameObject.SetActive(true);
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(ExitTutorial);


            // Hand animation
            tutorialUIObject.gameObject.SetActive(true);
            tutorialUIObject.transform.position = handFirstPos.position;
            _handAnimation = tutorialUIObject.transform.DOMove(handSecondPos.position, 1f).SetUpdate(true)
                .SetLoops(-1, LoopType.Restart);
            
            
            isTutorialShowed = true;
            _tutorialManager.SaveTutorialData();
            Time.timeScale = 0;
        }

        private void ExitTutorial()
        {
            _handAnimation?.Kill();
            Time.timeScale = 1;
            _tutorialManager.TutorialButton.onClick.RemoveAllListeners();
            _tutorialManager.TutorialButton.gameObject.SetActive(false);
            tutorialUIObject.gameObject.SetActive(false);
        }

        #endregion






    }
}