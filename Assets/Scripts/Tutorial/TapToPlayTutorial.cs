using System.Collections;
using Controllers;
using DG.Tweening;
using Levels;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

namespace Tutorial
{
    
    [System.Serializable]
    public class TapToPlayTutorial : BaseTutorial
    {
        
        #region PUBLIC FIELDS

        [Header("Tap To Play Tutorial Settings")] 
        [JsonIgnore] public string tutorialText; 
        [JsonIgnore] public Transform tutorialUIObject;
        [JsonIgnore] public Transform handPosition;

        #endregion
        
        #region PRIVATE FIELDS

        private Vector3 _baseUIScale;
        private Tween _handAnimation;

        #endregion
        
        #region OVERRIDE METHODS

        public override void MakeEventSubscriptions()
        {
            if (isTutorialShowed) return;

            LevelManager.OnLevelLoad += OnLevelLoadMethod;
            _tutorialManager.ManagerOnDestroy += () =>
            {
                LevelManager.OnLevelLoad -= OnLevelLoadMethod;
            };
            
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerator StartTutorial()
        {
            if(isTutorialShowed || _tutorialManager.IsTutorialsDisabled) yield break;
            
            yield return new WaitForSeconds(tutorialDelay);
            
                            

            // Text settings
            _tutorialManager.TutorialMainText.text = tutorialText;
            
            // Button settings
            var button = _tutorialManager.TutorialButton;
            button.gameObject.SetActive(true);
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(ExitTutorial);


            // Hand animation
            tutorialUIObject.gameObject.SetActive(true);
            tutorialUIObject.transform.position = handPosition.position;
            _baseUIScale = tutorialUIObject.localScale;
            var targetScale = _baseUIScale * .7f;
            _handAnimation = tutorialUIObject.DOScale(targetScale, .5f).SetUpdate(true).SetLoops(-1, LoopType.Yoyo);

            isTutorialShowed = true;
            _tutorialManager.SaveTutorialData();
            Time.timeScale = 0;

        }

        private void ExitTutorial()
        {
            _handAnimation?.Kill();
            Time.timeScale = 1;
            tutorialUIObject.localScale = _baseUIScale;
            _tutorialManager.TutorialButton.gameObject.SetActive(false);
            _tutorialManager.TutorialButton.onClick.RemoveAllListeners();
            tutorialUIObject.gameObject.SetActive(false);
        }
        
        private void OnLevelLoadMethod(Level level)
        {
            _tutorialManager.StartCoroutine(StartTutorial());
        }

        #endregion
        
    }
}