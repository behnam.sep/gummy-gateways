using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Tutorial
{
    [Serializable]
    public  class BaseTutorial
    {
        [Header("Base Tutorial Settings")]
        public int tutorialId;
        public float tutorialDelay;
        [HideInInspector] public bool isTutorialShowed;
        
        protected TutorialManager _tutorialManager;
        
        #region PUBLIC VIRTUAL METHODS

        public virtual void MakeEventSubscriptions() { }
        
        public virtual void SetTutorial(TutorialManager tutorialManager) => _tutorialManager = tutorialManager;

        #endregion


    }

    [Serializable]
    public class TutorialsDataWrapper
    { 
        public List<BaseTutorial> TutorialsList;
    }
    
}