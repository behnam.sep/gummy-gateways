using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Controllers;
using DG.Tweening;
using Levels;
using Storage;
using UnityEngine;
using UnityEngine.UI;

namespace Tutorial
{
    public class TutorialManager : MonoBehaviour
    {

        public Action ManagerOnDestroy;
        
        #region SERIALIZED PRIVATE FIELDS

        [SerializeField] private bool isTutorialsDisabled;

        [Header("References")] 
        [SerializeField] private SlotPowerUpsSpawner powerUpsSpawner;
        
        [Header("UI References")]
        [SerializeField] private Button tutorialButton;
        [SerializeField] private Text tutorialText;
        
        [Header("Tutorials")]
        [Header("Tap To Player Tutorial")] 
        [SerializeField] private TapToPlayTutorial tapToPlayTutorial;

        [Header("Positive pickup Tutorial")] 
        [SerializeField] private PositivePowerUpsTutorial positivePowerUpTutorial;
        
        [Header("Negative pickup Tutorial")] 
        [SerializeField] private NegativePowerUpsTutorial negativePowerUpTutorial;
        
        
        #endregion
        
        #region PRIVATE FIELDS

        private TutorialsDataWrapper _currentTutorialDataWrapper;
        private Vector3 _handBaseScale;
        

        #endregion

        #region PUBLIC FIELDS

        public Text TutorialMainText => tutorialText;
        public SlotPowerUpsSpawner PowerUpsSpawner => powerUpsSpawner;

        #endregion


        #region PUBLIC METHODS

        public Button TutorialButton => tutorialButton;
        public bool IsTutorialsDisabled => isTutorialsDisabled;

        #endregion
        
        #region UNITY METHODS

        private void Awake()
        {
            tutorialButton.gameObject.SetActive(false);
            LoadTutorialData();
            EventSubscriptions();
        }
        
        private void OnDestroy()
        {
            ManagerOnDestroy?.Invoke();
        }

        #endregion

        #region PRIVATE METHODS

        private void EventSubscriptions()
        {
            // Tap to play
            tapToPlayTutorial.SetTutorial(this);
            tapToPlayTutorial.MakeEventSubscriptions();
            
            // Positive Power up
            positivePowerUpTutorial.SetTutorial(this);
            positivePowerUpTutorial.MakeEventSubscriptions();
            
            // Negative PowerUp
            negativePowerUpTutorial.SetTutorial(this);
            negativePowerUpTutorial.MakeEventSubscriptions();
        }
        
        
        private void LoadTutorialData()
        {
            var loadedData = GameJsonDataController.GetTutorialData();
            if (loadedData == null)
            {
                _currentTutorialDataWrapper = new TutorialsDataWrapper
                {
                    TutorialsList = new List<BaseTutorial>
                    {
                        tapToPlayTutorial,
                        positivePowerUpTutorial,
                        negativePowerUpTutorial
                    }
                };
            }
            else
            {
                tapToPlayTutorial.isTutorialShowed = loadedData.TutorialsList.FirstOrDefault(t => t.tutorialId == tapToPlayTutorial.tutorialId).isTutorialShowed;
                positivePowerUpTutorial.isTutorialShowed = loadedData.TutorialsList.FirstOrDefault(t => t.tutorialId == positivePowerUpTutorial.tutorialId).isTutorialShowed;
                negativePowerUpTutorial.isTutorialShowed = loadedData.TutorialsList.FirstOrDefault(t => t.tutorialId == negativePowerUpTutorial.tutorialId).isTutorialShowed;
                
                _currentTutorialDataWrapper = new TutorialsDataWrapper
                {
                    TutorialsList = new List<BaseTutorial>
                    {
                        tapToPlayTutorial,
                        positivePowerUpTutorial,
                        negativePowerUpTutorial
                    }
                };
            }
            
            SaveTutorialData();
        }

        public void SaveTutorialData()
        {
            GameJsonDataController.SaveTutorialData(_currentTutorialDataWrapper);
        }
        
        #endregion



    }
}