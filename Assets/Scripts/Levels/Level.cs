using Controllers;
using UnityEngine;

namespace Levels
{
    public class Level : MonoBehaviour
    {
        #region PUBLIC FIELDS

        public int levelIndex;
        public int levelNumber;

        public int EnemyStartSpawnCount => enemyStartSpawnCount;
        public int PlayerStartSpawnCount => playerStartSpawnCount;
        
        public int StageCount => stageCount;
        
        #endregion
        
        #region SERIALIZE FIELDS
        [Header("Extra Information")] 
        [SerializeField] private int stageCount;

        [SerializeField] private int enemyStartSpawnCount;
        [SerializeField] private int playerStartSpawnCount;
        

        [SerializeField] private Material skyBox;
        [SerializeField] private Color fogColor;

        #endregion

        #region UNITY EVENT METHODS

        private void Awake()
        {
            RenderSettings.skybox = skyBox;
            RenderSettings.fogColor = fogColor;
        }

        #endregion
    }
}