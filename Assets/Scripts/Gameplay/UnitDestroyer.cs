using System;
using Controllers;
using UnityEngine;

namespace Gameplay
{
    public class UnitDestroyer : MonoBehaviour
    {
        private void OnTriggerStay(Collider other)
        {
            if (other.TryGetComponent(out UnitController unitController))
            {
                unitController.DestroyUnit();
            }
        }
    }
}