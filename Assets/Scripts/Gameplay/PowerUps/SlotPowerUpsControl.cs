using Controllers;
using DG.Tweening;
using UnityEngine;

namespace Gameplay.PowerUps
{
    public class SlotPowerUpsControl : MonoBehaviour
    {
        #region SERIALIZED PRIVATE FIELDS

        [SerializeField] private BasePowerUp powerUp;
        /*[SerializeField] private MeshRenderer targetMesh;
        [SerializeField] private Material[] activeMaterials;
        [SerializeField] private Material[] disabledMaterials;*/
        [SerializeField] private LayerMask wallLayer;
        [SerializeField] private float wallCheckRadiusParameter = .8f;
        #endregion
        
        #region PRIVATE FIELDS

        private float _settledYPosVal;
        private bool _isControllable;
        private SlotPowerUpsSpawner _spawner;
        private PowerUpSlots _mainPowerUpSlot;

        #endregion

        
        #region PUBLIC METHODS

        public void SetSlotPowerUp(SlotPowerUpsSpawner spawner,PowerUpSlots slot,float settledYValue)
        {
            _settledYPosVal = settledYValue;
            _mainPowerUpSlot = slot;
            slot.CurrentPowerUp = this;
            _spawner = spawner;
            _isControllable = true;
            //SetActiveMaterial();
        }
        
        public void TryAndStartControlling(out bool isControllable)
        {
            isControllable = _isControllable;
            if (_isControllable)
            {
                //SetDisabledMaterial();
                _isControllable = false;
            }
        }

        public void StopControlling(bool IsInPlayableGround)
        {
            if (IsInPlayableGround)
            {
                if(!Physics.CheckSphere(transform.position, wallCheckRadiusParameter, wallLayer))    
                {
                    _isControllable = false;
                    //SetActiveMaterial();
                    var pos = transform.position;
                    pos.y = _settledYPosVal;
                    transform.position = pos;
                    powerUp.PowerUpSettled();
                    _spawner.ControlledPowerUpSettled(_mainPowerUpSlot);
                    return;
                }
            }
            
            // Else go back to main slot pos and make it interactable.
            _isControllable = false;
            transform.DOMove(_mainPowerUpSlot.MainSpawnPos.position, .5f).OnComplete(() =>
            {
                _isControllable = true;
                //SetActiveMaterial();
            });
        }
        
        public void MoveToNewSlot(PowerUpSlots slot)
        {
            _mainPowerUpSlot = slot;
            _mainPowerUpSlot.CurrentPowerUp = this;
            _isControllable = false;
            transform.DOMove(slot.MainSpawnPos.position, .5f).SetEase(Ease.Linear).OnComplete(() =>
            {
                _isControllable = true;
            });
        }
        
        #endregion
        
        
        
        
    }
}