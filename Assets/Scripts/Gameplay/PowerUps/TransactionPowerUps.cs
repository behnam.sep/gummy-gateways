using System;
using Controllers;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Gameplay.PowerUps
{
    public class TransactionPowerUps : BasePowerUp
    {

        private enum TransactionType {Summation,Subtraction,Multiply,Divide}
        
        #region SERIALIZED PRIVATE FIELDS

        [Header("Transaction Power Up Parameters")] 
        [SerializeField] private TransactionType transactionType;
        [SerializeField] private float minValue;
        [SerializeField] private float maxValue;
        [SerializeField] private TextMeshProUGUI textObject;  // When value set change text value.

        #endregion


        #region PRIVATE FIELDS

        private bool _isUnitInteractionOn;
        private float _calcValue;

        #endregion

        #region UNITY METHODS

        private void Awake()
        {
            SetValueAndUI();
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if (!_isUnitInteractionOn) return;
            if (other.TryGetComponent(out UnitController unitController))
            {
                if (unitController.IsInteractionDisabled) return;
                _isUnitInteractionOn = false;
                MakeTransaction(unitController);
                Destroy(gameObject); 
            }
        }

        #endregion
        
        #region PUBLIC METHODS

        public override void PowerUpSettled()
        {
            base.PowerUpSettled();
            _isUnitInteractionOn = true;
        }
        
        public override void StopPowerUpAction()
        {
            _isUnitInteractionOn = false;
        }
        
        public void SetValueAndUI(float value = 0)
        {

            if (value == 0)
            {
                var randVal = Random.Range(minValue, maxValue);
                _calcValue = Mathf.Round(randVal);
            }
            else _calcValue = value;
            
            
            switch (transactionType)
            {
                case TransactionType.Summation:
                    textObject.text = $"+{_calcValue}";
                    break;
                case TransactionType.Subtraction:
                    textObject.text = $"-{_calcValue}";
                    break;
                case TransactionType.Multiply:
                    textObject.text = $"x{_calcValue}";
                    break;
                case TransactionType.Divide:
                    textObject.text = $"÷{_calcValue}";
                    break;
            }
        }

        #endregion
        
        #region PRIVATE METHODS

        private void MakeTransaction(UnitController unitController)
        {
            switch (transactionType)
            {
                case TransactionType.Summation:
                    SummationTransaction(unitController);
                    break;
                case TransactionType.Subtraction:
                    SubtractionTransaction(unitController);
                    break;
                case TransactionType.Multiply:
                    MultiplyTransaction(unitController);
                    break;
                case TransactionType.Divide:
                    DivideTransaction(unitController);
                    break;
            }
        }
        
        private void SummationTransaction(UnitController unitController)
        {
            unitController.UnitGroupController.AddUnits(unitController,(int)_calcValue);
        }

        // Belongs Transaction Power up
        private void SubtractionTransaction(UnitController unitController)
        {
            unitController.UnitGroupController.DestroyUnits(unitController,(int)_calcValue);
        }

        // Belongs Transaction Power up
        private void MultiplyTransaction(UnitController unitController)
        {
            var val = Mathf.Max(0, _calcValue - 1);
            var targetVal = (float)unitController.UnitGroupController.CurrentUnitCount * val;
            targetVal = Mathf.Round(targetVal);
            unitController.UnitGroupController.AddUnits(unitController,(int)targetVal);
        }
        
        // Belongs Transaction Power up
        private void DivideTransaction(UnitController unitController)
        {
            var currentUnitCount = (float)unitController.UnitGroupController.CurrentUnitCount;
            var targetVal = currentUnitCount - (currentUnitCount/_calcValue);
            targetVal = Mathf.Round(targetVal);
            unitController.UnitGroupController.DestroyUnits(unitController,(int)targetVal);
        }

        
        
        #endregion

        
        
        
    }
}