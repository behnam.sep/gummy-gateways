using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Gameplay.PowerUps
{
    public class DropPowerUp : MonoBehaviour
    {
        
        #region SERIALIZED PRIVATE FIELDS
        
        [SerializeField] private GameObject parachuteObject;
        [SerializeField] private GameObject spawnParticle;
        [SerializeField] private float spawnDelayAfterDrop = 1;
        [SerializeField] private float dropSpeed;
        
        
        #endregion

        #region PRIVATE FIELDS

        private BasePowerUp _selectedPowerUp;
        private Vector3 _targetLocation;
        private float _powerUpYPos;

        #endregion
        
        
        

        #region PUBLIC METHODS

        public void SetDropPowerUp(BasePowerUp selectedPowerUp, Vector3 targetLocation,float powerUpYPos)
        {
            _selectedPowerUp = selectedPowerUp;
            _targetLocation = targetLocation;
            _powerUpYPos = powerUpYPos;
            StartMovementToTargetLocation();
        }

        #endregion
        
        #region PRIVATE METHODS

        private void StartMovementToTargetLocation()
        {
            // TODO : Needs also animation while dropping

            transform.DOMove(_targetLocation, 1 / dropSpeed).SetEase(Ease.Linear).OnComplete(
                () =>
                {
                    StartCoroutine(DropSettled());
                }
            );
        }

        private IEnumerator DropSettled()
        {
            yield return new WaitForSeconds(.25f);
            Destroy(parachuteObject);
            yield return new WaitForSeconds(spawnDelayAfterDrop);
            // TODO needs drop particle.

            Instantiate(spawnParticle,transform.position,spawnParticle.transform.rotation);
            
            // Instantiate power up
            var targetPos = transform.position;
            targetPos.y = _powerUpYPos;
            var powerUp = Instantiate(_selectedPowerUp, targetPos, _selectedPowerUp.transform.rotation);
            powerUp.PowerUpSettled();
            
            Destroy(gameObject);
        }

        #endregion
    }
}