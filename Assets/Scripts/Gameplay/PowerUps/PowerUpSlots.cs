using Gameplay.PowerUps;
using UnityEngine;

namespace Gameplay.PowerUps
{
    public class PowerUpSlots : MonoBehaviour
    {
        

        #region SERIALIZED PRIVATE FIELDS
        
        [SerializeField] private Transform mainSpawnPos;
        //[SerializeField] private bool isMainSlot;
        
        #endregion


        #region PUBLIC FIELDS

        public Transform MainSpawnPos => mainSpawnPos;
        //public bool IsMainSlot => isMainSlot;
        public SlotPowerUpsControl CurrentPowerUp { get; set; }

        #endregion

        #region PUBLIC METHODS

        public void DestroyCurrentPowerUp()
        {
            if (CurrentPowerUp == null) return;
            Destroy(CurrentPowerUp.gameObject);
        }

        #endregion


    }
}