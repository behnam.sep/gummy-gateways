using System;
using System.Collections;
using Controllers;
using DG.Tweening;
using Unity.VisualScripting;
using UnityEngine;

namespace Gameplay.PowerUps
{
    public class BombPowerUp : BasePowerUp
    {

        #region SERIALIZED PRIVATE FIELDS

        [Header("Bomb Parameters")]
        [SerializeField] private Transform bombVisual;
        [SerializeField] private LayerMask unitsLayer;
        [SerializeField] private float bombTime = 2.5f;
        [SerializeField] private float bombRadius = 3;
        [SerializeField] private float animationScaleValue = 1.5f;
        [SerializeField] private float animationSpeed = 5;
        [SerializeField] private GameObject explosionParticle;
        [SerializeField] private Transform explosionParticlePos;
        
        
        #endregion


        #region PRIVATE FIELDS

        private Tween _bombAnimation;
        

        #endregion
        

        #region UNITY METHODS
#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position,bombRadius);
        }
        
#endif

        #endregion
        
        #region PUBLIC METHODS

        public override void PowerUpSettled()
        {
            base.PowerUpSettled();
            
            // Start bomb timer and animation.
            StartCoroutine(StartExplosionTimer());
        }
        
        public override void StopPowerUpAction()
        {
            
        }

        #endregion

        #region PRIVATE METHODS

        private IEnumerator StartExplosionTimer()
        {
            // Bomb animation.
            _bombAnimation = bombVisual.DOScale(bombVisual.localScale * animationScaleValue, 1 / animationSpeed).SetEase(Ease.Linear).SetLoops(-1,LoopType.Yoyo);
            yield return new WaitForSeconds(bombTime);
            _bombAnimation?.Kill();
            
            // Kill units in radius.
            var unitsColliders = Physics.OverlapSphere(transform.position, bombRadius, unitsLayer);
            foreach (var unitCollider in unitsColliders)
            {
                var unit = unitCollider.GetComponent<UnitController>();
                unit.DestroyUnit();
            }
            
            // Explosion particle.
            Instantiate(explosionParticle, explosionParticlePos.position, explosionParticle.transform.rotation);
            Destroy(gameObject);
        }
        
        
        #endregion
    }
}