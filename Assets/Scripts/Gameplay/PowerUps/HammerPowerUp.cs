using System;
using System.Numerics;
using Controllers;
using DG.Tweening;
using Unity.Mathematics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace Gameplay.PowerUps
{
    public class HammerPowerUp : BasePowerUp
    {

        #region SERIALIZED PRIVATE FIELDS

        [Header("Hammer Parameters")] 
        
        [SerializeField] private Transform hammerHead;
        [SerializeField] private LayerMask unitLayer;
        [SerializeField] private Transform rightHitPos;
        [SerializeField] private Transform rightTarget;
        [SerializeField] private Transform leftHitPos;
        [SerializeField] private Transform leftTarget;
        [SerializeField] private float hitRadius;
        [SerializeField] private float hammerRotationSpeed = 1;
        [SerializeField] private float delayBetweenHits = .15f;
        
        #endregion



        #region PRIVATE FIELDS
        
        private Transform _currentTargetHitPoint;
        private Transform _currentTarget;
        private bool _isRotationOn;
        private bool _isGoingRight;
        private bool _isHammerActionOn;

        #endregion
        
        #region UNITY METHODS

        private void Update()
        {
            HammerRotationHandle();
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(rightHitPos.position,hitRadius);
            Gizmos.DrawWireSphere(leftHitPos.position,hitRadius);
        }
        
        #endif
        
        #endregion
        
        #region PUBLIC METHODS

        public override void PowerUpSettled()
        {
            base.PowerUpSettled();
        
            // Start hammer action.
            _isHammerActionOn = true;
            HandleHammerAction();
        }
        
        public override void StopPowerUpAction()
        {
            _isHammerActionOn = false;
        }
        
        #endregion

        #region PRIVATE METHODS
        
        private void HandleHammerAction()
        {
            if (!_isHammerActionOn) return;
            
            // Update values.
            _isGoingRight = !_isGoingRight;
            _currentTargetHitPoint = _isGoingRight ? rightHitPos : leftHitPos;
            _currentTarget = _isGoingRight ? rightTarget : leftTarget;
            
            Invoke(nameof(StartRotation),delayBetweenHits);
        }

        private void StartRotation() => _isRotationOn = true;

        
        private void HammerRotationHandle()
        {
            if (!_isRotationOn) return;
            
            var dir = _isGoingRight ? -1 : 1;
            hammerHead.Rotate(Vector3.right,dir*hammerRotationSpeed*Time.deltaTime);
            var targetYDiff = Mathf.Abs(_currentTargetHitPoint.position.y - _currentTarget.position.y);
            if (targetYDiff < .1f || _currentTargetHitPoint.position.y < _currentTarget.position.y)
            {
                _isRotationOn = false;
                var units =Physics.OverlapSphere(_currentTargetHitPoint.position,hitRadius,unitLayer);
                foreach (var unit in units)
                {
                    unit.GetComponent<UnitController>().DestroyUnit();
                }
                // Maybe we can add particle like some dust when hit the ground.
                HandleHammerAction();
            }
        }
        
        #endregion
    }
}