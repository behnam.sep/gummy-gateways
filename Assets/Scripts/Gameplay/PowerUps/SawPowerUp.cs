using System;
using Controllers;
using DG.Tweening;
using UnityEngine;

namespace Gameplay.PowerUps
{
    public class SawPowerUp : BasePowerUp
    {

        #region SERIALIZED PRIVATE FIELDS

        [Header("Saw settings")] 
        [SerializeField] private Transform rotateObject;
        [SerializeField] private float rotateTime = 1;

        #endregion
        
        
        #region PRIVATE FIELDS

        private bool _isUnitInteractionOn;

        #endregion

        
        #region UNITY METHODS

        private void OnTriggerEnter(Collider other)
        {
            if (!_isUnitInteractionOn) return;
            if (other.TryGetComponent(out UnitController unitController))
            {
                if (unitController.IsInteractionDisabled) return;
                unitController.DestroyUnit();
                Destroy(gameObject); 
            }
        }

        #endregion
        

        #region PUBLIC METHODS

        public override void PowerUpSettled()
        {
            base.PowerUpSettled();
            _isUnitInteractionOn = true;
            StartSawAnimation();
        }
        
        public override void StopPowerUpAction()
        {
            _isUnitInteractionOn = false;
        }

        #endregion

        #region PRIVATE METHODS

        private  void StartSawAnimation()
        {
            var targetRot = rotateObject.eulerAngles;
            targetRot.y = 359;
            rotateObject.DORotate(targetRot, rotateTime,RotateMode.FastBeyond360).SetLoops(-1).SetEase(Ease.Linear);
        }

        #endregion
        
        
    }
}