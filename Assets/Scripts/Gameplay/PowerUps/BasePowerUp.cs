using System;
using Controllers;
using UnityEngine;

namespace Gameplay.PowerUps
{
    public abstract class BasePowerUp : MonoBehaviour
    {
        #region PRIVATE/PROTECTED FIELDS
        
        protected bool _isSettled;

        #endregion


        #region UNITY METHODS

        protected virtual void Start()
        {
            GameplayStageManager.Instance.OnStageEnds += OnStageEndsMethod;
        }

        protected virtual void OnDestroy()
        {
            GameplayStageManager.Instance.OnStageEnds -= OnStageEndsMethod;
        }

        #endregion


        #region PUBLIC/ABSTRACT METHODS

        public virtual void PowerUpSettled()
        {
            _isSettled = true;
        }
        
        public abstract void StopPowerUpAction();

        #endregion

        #region PRIVATE/PROTECTED METHODS

        private void OnStageEndsMethod()
        {
            if (_isSettled)
            {
                Destroy(gameObject);
            }
        }
        #endregion
        
    }
}