using System;
using Controllers;
using DG.Tweening;
using UnityEngine;

namespace Gameplay.PowerUps
{
    public class TrapPowerUp : BasePowerUp
    {

        #region SERIALIZED PRIVATE FIELDS

        [SerializeField] private Transform upperTrapPart;
        [SerializeField] private Transform lowerTrapPart;
        [SerializeField] private float closedAngle = 45;
        [SerializeField] private float animationSec = .3f;
        
        #endregion
        

        #region PRIVATE FIELDS
        
        private bool _isInteractionOn;
        

        #endregion



        #region UNITY METHODS

        private void OnTriggerEnter(Collider other)
        {
            if (!_isInteractionOn) return;
            if (other.TryGetComponent(out UnitController unitController))
            {
                if (unitController.IsInteractionDisabled) return;
                unitController.DestroyUnit();
                _isInteractionOn = false;
                // Start trap animation.
                StartTrapAnimation();
            }
        }

        #endregion
        
        #region PUBLIC METHODS

        public override void PowerUpSettled()
        {
            base.PowerUpSettled();
            _isInteractionOn = true;
        }
        
        public override void StopPowerUpAction()
        {
            _isInteractionOn = false;
        }

        #endregion

        #region PRIVATE METHODS

        private void StartTrapAnimation()
        {
            var relativeAngle = -Vector3.right * closedAngle;
            upperTrapPart.DOLocalRotate(relativeAngle, animationSec,RotateMode.LocalAxisAdd);
            lowerTrapPart.DOLocalRotate(relativeAngle, animationSec,RotateMode.LocalAxisAdd).OnComplete(() =>
            {
                Destroy(gameObject,.5f);
            });
        }

        #endregion
        
       
    }
}