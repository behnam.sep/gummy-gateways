using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Gameplay
{
    public class SplatParticleControl : MonoBehaviour
    {
        [SerializeField] private Transform splatMesh;
        [SerializeField] private float destroySec;
        [SerializeField] private float scaleEffectTime = .5f;
        [SerializeField] private float scaleMultiplier = .5f;


        private void Start()
        {
            var baseScale = splatMesh.localScale;
            splatMesh.localScale *= scaleMultiplier;
            splatMesh.DOScale(baseScale, scaleEffectTime);
            StartCoroutine(DeathTimer());
        }


        private IEnumerator DeathTimer()
        {
            yield return new WaitForSeconds(destroySec);
            Destroy(gameObject);
        }


    }
}