using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

namespace Gameplay
{
    public class GameArena : MonoBehaviour
    {
        #region SERIALIZED PRIVATE FIELDS
        
        [SerializeField] private int arenaId;
        [SerializeField] private Transform[] enemySpawnPoints;
        [SerializeField] private Transform[] playerSpawnPoints;
        [SerializeField] private GameObject frontGate;
        [SerializeField] private GameObject backGate;
        [SerializeField] private NavMeshObstacle backGateObstacle;
        [SerializeField] private Transform frontDoor;
        [SerializeField] private float doorAnimSec = 1;
        [SerializeField] private Collider[] unitDestroyerColliders;
        [SerializeField] private Collider[] wallsColliders;
        [SerializeField] private NavMeshObstacle[] navmeshObstacles;
        [Header("Drop area Information")] 
        [SerializeField] private DropAreas[] dropAreas;
       

        #endregion
        
        #region PUBLIC FIELDS

        public Transform[] EnemySpawnPoints => enemySpawnPoints;
        public Transform[] PlayerSpawnPoints => playerSpawnPoints;
        public GameObject FrontGate => frontGate;
        public GameObject BackGate => backGate;
        public NavMeshObstacle BackGateObstacle => backGateObstacle;
        public DropAreas[] DropAreas => dropAreas;
        public int ArenaId => arenaId;

        #endregion


#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            foreach (var dropArea in dropAreas)
            {
                Gizmos.DrawWireSphere(dropArea.centerPosition.position,dropArea.areaRadius);
            }
        }


#endif


        #region PUBLIC METHODS

        public void EnableUnitDestroyerColliders()
        {
            foreach (var dCollider in unitDestroyerColliders)
                dCollider.enabled = true;
        }

        public void DisableUnitDestroyerColliders()
        {
            foreach (var dCollider in unitDestroyerColliders)
                dCollider.enabled = false;
        }

        public void EnableWallColliders()
        {
            foreach (var wCollider in wallsColliders)
                wCollider.enabled = true;
        }

        public void DisableWallColliders()
        {
            foreach (var wCollider in wallsColliders)
                wCollider.enabled = false;
        }

        public void EnableNavmeshObstacles()
        {
            foreach (var obstacle in navmeshObstacles)
            {
                obstacle.enabled = true;
            }
        }

        public void DisableNavmeshObstacles()
        {
            foreach (var obstacle in navmeshObstacles)
            {
                obstacle.enabled = false;
            }
        }

        public void OpenDoorAnimation(Action OnAnimationEnds)
        {
            frontDoor.DOLocalRotate(Vector3.zero, doorAnimSec).OnComplete(
                () =>
                {
                    OnAnimationEnds?.Invoke();
                }
                );
        }

        public void CloseDoorAnimation()
        {
            frontDoor.DOLocalRotate(Vector3.up * 90, doorAnimSec);
        }
        
        
        #endregion
    }

    [Serializable]
    public class DropAreas
    {
        public Transform centerPosition;
        public float areaRadius = 1;
    }
}