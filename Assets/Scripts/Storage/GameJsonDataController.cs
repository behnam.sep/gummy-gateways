using Tutorial;
using UnityEngine;

namespace Storage
{
    public static class GameJsonDataController
    {
        
        public static void SaveTutorialData(TutorialsDataWrapper tutorialData)
        {
            var jsonVal = JsonUtility.ToJson(tutorialData);
            PlayerPrefsController.SetTutorialData(jsonVal);
        }

        public static TutorialsDataWrapper GetTutorialData()
        {
            var jsonData = PlayerPrefsController.GetTutorialData();
            if (jsonData.Length < 10)
                return null;
            return JsonUtility.FromJson<TutorialsDataWrapper>(jsonData);
            
        }
        
    }
}