using System;
using Levels;
using UnityEngine;

namespace Controllers
{
    public class UnitController : MonoBehaviour
    {

        public Action OnUnitDestroy;

        #region SERIALIZED PRIVATE FIELDS

        [SerializeField] private UnitMover unitMover;
        [SerializeField] private UnitInteractionController unitInteractionController;
        [SerializeField] private Transform destroyParticle;
        [SerializeField] private Transform particleSpawnPos;
        
        
        #endregion


        #region PUBLIC FIELDS

        public UnitsGroupController UnitGroupController { get; private set; }
        public bool IsInteractionDisabled { get; private set; }
        public UnitMover UnitMover => unitMover;
        
        #endregion

        #region UNITY METHODS

        private void Start()
        {
            GameplayStageManager.Instance.OnStageEnds += OnStageEndsMethod;
            GameplayStageManager.Instance.OnStageSettled += OnStageSettled;
        }
        
        private void OnDestroy()
        {
            GameplayStageManager.Instance.OnStageEnds -= OnStageEndsMethod;
            GameplayStageManager.Instance.OnStageSettled -= OnStageSettled;
        }

        private void Update()
        {
            var eulerAngle = transform.eulerAngles;
            eulerAngle.x = 0;
            eulerAngle.z = 0;
            transform.eulerAngles = eulerAngle;
        }

        #endregion

        #region PUBLIC METHODS

        public void DestroyUnit()
        {
            Instantiate(destroyParticle, particleSpawnPos.position, destroyParticle.rotation);
            UnitGroupController.UnitDestroyed(this);
            OnUnitDestroy?.Invoke();
            Destroy(gameObject);
        }

        public void SetUnitController(UnitsGroupController unitsGroupController)
        {
            UnitGroupController = unitsGroupController;
        }

        // When game starts this will be called, also when game is on and new unit instantiated this also will be call.
        public void StartUnitController()
        {
            unitMover.StartMovement();
            unitInteractionController.StartAndSetInteractionController();
        }

        public void StopUnitController()
        {
            unitMover.StopMovement();
            unitInteractionController.StopUnitInteraction();
        }

        public void DisableTransactionInteractionByTime()
        {
            IsInteractionDisabled = true;
            Invoke(nameof(EnableTransactionInteraction),1);
        }
        
        
        #endregion
        
        #region PRIVATE METHODS

        
        
        private void OnStageSettled()
        {
           StartUnitController();
        }

        private void OnStageEndsMethod()
        {
            StopUnitController();
        }

        private void EnableTransactionInteraction() => IsInteractionDisabled = false;

        #endregion


    }
}