using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

namespace Controllers
{
    public class UnitMover : MonoBehaviour
    {

        [SerializeField] private Animator animator; // Add animator events.
        [SerializeField] private Rigidbody rigidbody;
        [SerializeField] private NavMeshAgent nmAgent;
        [SerializeField] private float speed = 5;




        public bool IsMovingOn { get; private set; }


        #region PRIVATE METHODS

        private Action _onMovementEndsAction;
        private Transform _currentTarget;
        private bool _isNavmeshMovingOn;
        private static readonly int Running = Animator.StringToHash("running");

        #endregion
        

        #region UNITY METHODS
        
        private void Update()
        {
            if (IsMovingOn)
            {
                var velocity = transform.forward * speed;
                velocity.y = rigidbody.velocity.y;
                rigidbody.velocity = velocity;
            }
            
            if(_isNavmeshMovingOn)
                HandleNavmeshMoving();
        }

        #endregion
        
        #region PUBLIC METHODS

        public void StartMovement()
        {
            animator.SetBool(Running,true);
            IsMovingOn = true;
            rigidbody.isKinematic = false;
        }

        public void StopMovement()
        {
            animator.SetBool(Running,false);
            IsMovingOn = false;
            rigidbody.velocity = Vector3.zero;
            rigidbody.isKinematic = true;
        }

        public void StartNavMeshMovement(Transform target,Action OnMovementEnds)
        {
            nmAgent.enabled = true;
            _isNavmeshMovingOn = true;
            _currentTarget = target;
            _onMovementEndsAction = OnMovementEnds;
            nmAgent.isStopped = false;
            animator.SetBool(Running,true);
            nmAgent.SetDestination(target.position);
        }
        
        #endregion

        #region PRIVATE METHODS

        private void HandleNavmeshMoving()
        {
            if (nmAgent.pathPending) return;
            if (nmAgent.remainingDistance <= .1f)
            {
                NavigationMovementEnds();
            }
        }

        private void NavigationMovementEnds()
        {
            animator.SetBool(Running,false);
            nmAgent.isStopped = true;
            _isNavmeshMovingOn = false;
            nmAgent.enabled = false;
            transform.DORotateQuaternion(_currentTarget.rotation, .25f).OnComplete(() =>
            {
                _onMovementEndsAction?.Invoke();
            });
        }

        #endregion
        
        
    }
}