using System;
using System.Collections.Generic;
using Gameplay;
using Gameplay.PowerUps;
using Levels;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Controllers
{
    public class SlotPowerUpsSpawner : MonoBehaviour
    {


        public Action<int> OnPowerUpSelection;
        public Action<SlotPowerUpsControl,int> OnPowerUpSpawn;

        
        #region SERIALIZED PRIVATE FIELDS

        [SerializeField] private GameObject slotsParentObject;
        [SerializeField] private SlotPowerUpsData[] slotPowerUpsData;
        [SerializeField] private List<PowerUpSlots> powerUpSlots;
        [SerializeField] private Transform newSpawnPosition;
        [SerializeField] private float _settledYPosValue = -0.56f;
        
        #endregion
        
        
        #region PRIVATE FIELDS
        
        private int _spawnCount;
        private bool _isTimerOn;
        private float _timer;

        #endregion


        public SlotPowerUpsControl CurrentSelectedPrefab { get; set; }
        
        #region UNITY METHODS

        private void Start()
        {
            GameplayStageManager.Instance.OnStageSettled += OnStageSettledMethod;
            GameplayStageManager.Instance.OnStageEnds += OnStageEndsMethod;
        }

        private void OnDestroy()
        {
            GameplayStageManager.Instance.OnStageSettled -= OnStageSettledMethod;
            GameplayStageManager.Instance.OnStageEnds -= OnStageEndsMethod;
        }
        
        #endregion

        #region PRIVATE METHODS
        
        private void SpawnTransactionPowerUpAtSlot(PowerUpSlots slot,bool isInstant)
        {
            
            
            _spawnCount++;
            CurrentSelectedPrefab = null;
            OnPowerUpSelection?.Invoke(_spawnCount);

            if ( CurrentSelectedPrefab == null)
            {
                float totalPriorityVal = 0;
                foreach (var slotPowerUp in slotPowerUpsData)
                {
                    totalPriorityVal += slotPowerUp.currentPriority;
                }
            
                var randVal = Random.Range(0, totalPriorityVal);
                float cumulativeVal = 0;
            
                SlotPowerUpsData selectedPowerUpData = null;
                foreach (var powerUp in slotPowerUpsData)
                {
                    cumulativeVal += powerUp.currentPriority;
                    if (randVal <= cumulativeVal)
                    {
                        //powerUp.priority = Mathf.Max(0, powerUp.priority - priorityChangeValue);
                        powerUp.currentPriority = powerUp.basePriorityValue;
                        selectedPowerUpData = powerUp;
                        break;
                    }
                }

                CurrentSelectedPrefab = selectedPowerUpData.powerUp;

                foreach (var powerUpData in slotPowerUpsData)
                {
                    if (powerUpData != selectedPowerUpData)
                    {
                        powerUpData.currentPriority += powerUpData.increasePriorityValue;
                    }
                }
            }
            
            // Spawn power up.
            if (isInstant)
            { 
                var newPowerUp = Instantiate( CurrentSelectedPrefab, slot.MainSpawnPos.position,  CurrentSelectedPrefab.transform.rotation); 
                newPowerUp.transform.SetParent(slot.MainSpawnPos); 
                newPowerUp.SetSlotPowerUp(this,slot,_settledYPosValue);
                OnPowerUpSpawn?.Invoke(newPowerUp,_spawnCount);
            }
            
            else 
            { 
                var newPowerUp = Instantiate( CurrentSelectedPrefab, newSpawnPosition.position,  CurrentSelectedPrefab.transform.rotation); 
                newPowerUp.transform.SetParent(slot.MainSpawnPos); 
                newPowerUp.SetSlotPowerUp(this,slot,_settledYPosValue); 
                newPowerUp.MoveToNewSlot(slot); 
                OnPowerUpSpawn?.Invoke(newPowerUp,_spawnCount);
            }
            
        }
        
        private void OnStageEndsMethod() => slotsParentObject.SetActive(false);
        
        private void OnStageSettledMethod()
        {
            slotsParentObject.SetActive(true);
            foreach (var slot in powerUpSlots)
            {
                slot.DestroyCurrentPowerUp();
                SpawnTransactionPowerUpAtSlot(slot,true);
            }
        }
        
        #endregion
        
        #region PUBLIC METHODS

        public void ControlledPowerUpSettled(PowerUpSlots slot)
        {
            var slotIndex = powerUpSlots.IndexOf(slot);
            // Move existing power ups to new positions.
            for (int i = slotIndex+1; i < powerUpSlots.Count; i++)
            {
                var powerUp = powerUpSlots[i].CurrentPowerUp;
                var newSlot = powerUpSlots[i - 1];
                powerUp.MoveToNewSlot(newSlot);
            }
            
            // Create new power up.
            var lastSlot = powerUpSlots[^1];
            SpawnTransactionPowerUpAtSlot(lastSlot,false);
        }

        #endregion
        
    }

    [System.Serializable]
    public class SlotPowerUpsData
    {
        public SlotPowerUpsControl powerUp;
        public float currentPriority;
        public float basePriorityValue = 5;
        public float increasePriorityValue = 5;
    }
}