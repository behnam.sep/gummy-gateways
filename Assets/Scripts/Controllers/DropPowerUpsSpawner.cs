using UnityEngine;
using Gameplay;
using Gameplay.PowerUps;
using Levels;

namespace Controllers
{
    public class DropPowerUpsSpawner : MonoBehaviour
    {
        [SerializeField] private DropPowerUp dropPowerUpPrefab;
        [SerializeField] private BasePowerUp[] powerUps;
        [SerializeField] private float spawnInterval = 5f;
        [SerializeField] private float powerUpSpawnHeight = .5f;
        [SerializeField] private float spawnHeight = 10f;
        [SerializeField] private float groundYPosition = .5f;

        private DropAreas[] _dropAreas;
        private float _spawnTimer;
        private bool _isSpawningActive;

        private void Awake()
        {
            SubscribeToGameplayEvents();
        }

        private void Start()
        {
            _isSpawningActive = false; // Initially, spawning is inactive until the stage is settled.
        }

        private void Update()
        {
            HandleSpawnTimer();
        }

        private void OnDestroy()
        {
            UnsubscribeFromGameplayEvents();
        }

        private void SubscribeToGameplayEvents()
        {
            var manager = GameplayStageManager.Instance;
            manager.OnStageArenaSpawn += OnStageArenaSpawn;
            manager.OnStageEnds += OnStageEnds;
            manager.OnStageSettled += OnStageSettled;
        }

        private void UnsubscribeFromGameplayEvents()
        {
            var manager = GameplayStageManager.Instance;
            if (manager != null)
            {
                manager.OnStageEnds -= OnStageEnds;
                manager.OnStageSettled -= OnStageSettled;
                manager.OnStageArenaSpawn -= OnStageArenaSpawn;
            }
        }

        private void OnStageArenaSpawn(int remainingPlayerUnitCount, int stageLevel, GameArena arena, Level level)
        {
            _dropAreas = arena.DropAreas;
        }

        private void OnStageSettled()
        {
            StartSpawning();
        }

        private void OnStageEnds()
        {
            StopSpawning();
        }

        private void StartSpawning()
        {
            _isSpawningActive = true;
            _spawnTimer = 0; // Reset the timer when starting spawning.
        }

        private void StopSpawning()
        {
            _isSpawningActive = false;
            // Optionally, implement logic to clean up or reset after stopping.
        }

        private void HandleSpawnTimer()
        {
            if (!_isSpawningActive) return;

            _spawnTimer += Time.deltaTime;
            if (_spawnTimer >= spawnInterval)
            {
                SpawnDropPowerUp();
                _spawnTimer = 0; // Reset timer after spawn.
            }
        }

        private void SpawnDropPowerUp()
        {
            if (powerUps.Length == 0 || _dropAreas.Length == 0) return;

            BasePowerUp selectedPowerUp = powerUps[Random.Range(0, powerUps.Length)];
            DropAreas selectedDropArea = _dropAreas[Random.Range(0, _dropAreas.Length)];
            Vector2 randCircle = Random.insideUnitCircle * selectedDropArea.areaRadius;
            Vector3 dropPosition = selectedDropArea.centerPosition.position + new Vector3(randCircle.x, groundYPosition, randCircle.y);
            Vector3 spawnPosition = new Vector3(dropPosition.x, spawnHeight, dropPosition.z);

            DropPowerUp spawnedDropPowerUp = Instantiate(dropPowerUpPrefab, spawnPosition, Quaternion.identity);
            spawnedDropPowerUp.SetDropPowerUp(selectedPowerUp, dropPosition, powerUpSpawnHeight);
        }
    }
}
