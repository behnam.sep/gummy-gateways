using System;
using System.Collections.Generic;
using Dreamteck.Splines.Primitives;
using Gameplay;
using Levels;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Controllers
{
    public abstract class UnitsGroupController : MonoBehaviour
    {

        #region SERIALIZED PRIVATE FIELDS

        
        [SerializeField] private UnitController unitPrefab;
        [Header("Start Spawn")]
        [SerializeField] protected int startUnitCount;
        [Header("Dynamic spawn")] 
        [SerializeField] private float spawnEverySec;
        [SerializeField] protected int baseMaxSpawnCount = 4;
        [SerializeField] protected float levelIncSpawnCount = .5f;
        [SerializeField] private float widthRange = 2f;
        
        
        #endregion
        
        #region PRIVATE FIELDS

        protected List<UnitController> _activeUnits;
        protected Transform[] _arenaSpawnPositions;
        protected bool _isTimerOn;
        protected float _timer;
        protected int _currentMaxSpawnCount;
        private int _spawnCount;

        #endregion

        #region PUBLIC FIELDS

        public int CurrentUnitCount => _activeUnits.Count;

        #endregion
        
        #region UNITY METHODS

        protected void Awake()
        {
            _activeUnits = new List<UnitController>();
            
            GameplayStageManager.Instance.OnStageSettled += OnStageSettledMethod;
            GameplayStageManager.Instance.OnStageArenaSpawn += OnStageArenaSpawnMethod;
            GameplayStageManager.Instance.OnStageEnds += OnStageEndsMethod;
        }
        
        protected virtual void Update()
        {
            if (_isTimerOn)
            {
                _timer += Time.deltaTime;
                if (_timer >= spawnEverySec)
                {
                    _timer = 0;
                    // Spawn unit.
                    SpawnUnitByTime();
                }
            }
        }

        private void OnDisable()
        {
            GameplayStageManager.Instance.OnStageSettled -= OnStageSettledMethod;
            GameplayStageManager.Instance.OnStageArenaSpawn -= OnStageArenaSpawnMethod;
            GameplayStageManager.Instance.OnStageEnds -= OnStageEndsMethod;
        }

        #endregion

        #region PROTECTED METHODS
        
        protected virtual void SpawnUnitsAtSpawnPos(float spawnCount)
        {

            for (int i = 0; i < spawnCount; i++)
            {
                var dirMultiplier = i % 2 == 1 ? 1 : -1;
                float rowOffset = 0;
                
                Vector3 targetPos = _arenaSpawnPositions[i].position;
                //targetPos.z +=  -Mathf.Sign(transform.forward.z) * zOffset;
                var newUnit = Instantiate(unitPrefab, targetPos, transform.rotation);
                newUnit.SetUnitController(this);
                newUnit.transform.SetParent(transform);
                _activeUnits.Add(newUnit);
                UpdateCountUI();
                
            }
        }
        protected virtual void SpawnUnitByTime()
        {
            if (_spawnCount >= _currentMaxSpawnCount) return;
            float randWidth = Random.Range(-widthRange, widthRange);
            var targetTransform = _arenaSpawnPositions[0];
            var targetPos = targetTransform.position + (targetTransform.right * randWidth);
            //Instantiate(spawnParticle, targetPos, spawnParticle.transform.rotation);    // Spawn particle.
            var unit = Instantiate(unitPrefab, targetPos, transform.rotation);
            unit.SetUnitController(this);
            unit.StartUnitController();
            unit.DisableTransactionInteractionByTime();
            unit.transform.SetParent(transform);
            _activeUnits.Add(unit);
            UpdateCountUI();
            _spawnCount++;
        }
        
        protected virtual void StartTimer()
        {
            _spawnCount = 0;
            _timer = 0;
            _isTimerOn = true;
        }
        
        protected virtual void StopSpawnTimer()
        {
            _isTimerOn = false;
        }

        // On stage settled.
        protected virtual void OnStageSettledMethod()
        {
            
            StartTimer();
        }
        
        protected virtual void OnStageEndsMethod()
        {
            StopSpawnTimer();
        }
        
        #endregion

        #region PUBLIC METHODS
        
        public virtual void AddUnits(UnitController interactedUnit, int value)
        {
            var unitTransform = interactedUnit.transform;
            value = Mathf.Max(1, value);
            for (int i = 0; i < value; i++)
            {
                var dir = i % 2 == 0 ? 1 : -1;
                var targetPos = unitTransform.position + ((unitTransform.right) *.5f * dir );
                targetPos.x = Random.Range(targetPos.x - .1f, targetPos.x + .1f);
                targetPos.z = Random.Range(targetPos.z - .1f, targetPos.z + .1f);
                //Instantiate(spawnParticle, targetPos, spawnParticle.transform.rotation);    // Spawn particle.
                var unit = Instantiate(unitPrefab, targetPos, interactedUnit.transform.rotation);
                unit.SetUnitController(this);
                unit.StartUnitController();
                unit.DisableTransactionInteractionByTime();
                _activeUnits.Add(unit);
                UpdateCountUI();
                unit.transform.SetParent(transform);
            }
        }

        public virtual void DestroyUnits(UnitController interactedUnit, int value)
        {
            interactedUnit.DestroyUnit();
            var clampedVal = Mathf.Min(value-1, CurrentUnitCount);
            for (int i = 0; i < clampedVal; i++)
            {
                var randUnitIndex = Random.Range(0, CurrentUnitCount);
                _activeUnits[randUnitIndex].DestroyUnit();
                UpdateCountUI();
            }
        }
        
        // Calling from unit when it's destroyed.
        public void UnitDestroyed(UnitController unit)
        {
            _activeUnits.Remove(unit);
            UpdateCountUI();
            if (_activeUnits.Count == 0)
            {
                AllUnitsDestroyed();
            }
        }
        
        #endregion
        
        #region ABSTRACT METHODS

        protected abstract void AllUnitsDestroyed();
        
        protected abstract void UpdateCountUI();
        
        protected abstract void OnStageArenaSpawnMethod(int nextStagePlayerUnits,int stageLevel, GameArena arenaSpawned,Level level);

        #endregion

    }
}