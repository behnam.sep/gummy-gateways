using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Controllers
{
    public class UnitInteractionController : MonoBehaviour
    {
        public enum UnitTypes {Player,Enemy}

        #region SERIALIZED PRIVATE FIELDS

        [SerializeField] private UnitController unitController;
        [SerializeField] private UnitTypes unitType;
        [SerializeField] private Transform rayStartPos;
        [SerializeField] private float rayLength;
        [SerializeField] private float sphereRayRadius = 1;
        [SerializeField] private float minDirectionAngle = 10;
        [SerializeField] private LayerMask wallLayer;

        #endregion

        public bool showLogs;
        
        public UnitTypes UnitType => unitType;

        #region PRIVATE FIELDS

        private bool _isRayCastOn = true;
        private bool _isCollisionOn = true;
        private bool _isInteractionActive;

        #endregion
        
        #region UNITY METHODS
        
        private void FixedUpdate()
        {
            if (!_isRayCastOn || !_isInteractionActive) return;
            if (Physics.SphereCast(rayStartPos.position,sphereRayRadius, transform.forward, out RaycastHit hitInfo, rayLength,wallLayer))
            {
                if (showLogs)
                {
                    Debug.Log("Hit with wall collider turn");
                }
                
                var angle = Vector3.SignedAngle(hitInfo.normal, -transform.forward, Vector3.up);
                    transform.forward = -transform.forward;
                    if (Mathf.Abs(angle) < minDirectionAngle)
                    {
                        angle = Mathf.Sign(angle) * minDirectionAngle;
                    }
                    transform.Rotate(Vector3.up, -2 * angle);
                    _isRayCastOn = false;
                    Invoke(nameof(SetRayCastOn),.2f);
            }
        }

        /*private void OnCollisionEnter(Collision collision)
        {
            if (!_isInteractionActive || !_isCollisionOn) return;
            if (collision.gameObject.TryGetComponent(out UnitInteractionController unit))
            {
                if (unit.UnitType == unitType)
                {
                    // Change direction back.
                    _isCollisionOn = false;
                    //Invoke(nameof(SetCollisionOn),.2f);
                    transform.forward = collision.GetContact(0).normal;
                }
                else
                {
                   unitController.DestroyUnit();
                }
            }
        }*/

        private void OnCollisionStay(Collision collisionInfo)
        {
            if (!_isInteractionActive || !_isCollisionOn) return;
            if (collisionInfo.gameObject.TryGetComponent(out UnitInteractionController unit))
            {
                if (unit.UnitType == unitType)
                {
                    //Debug.Log("Unit collision stay");
                    // Change direction back.
                    _isCollisionOn = false;
                    Invoke(nameof(SetCollisionOn),.2f);
                    transform.forward = collisionInfo.GetContact(0).normal;
                    //transform.forward = -transform.forward;
                }
                else
                {
                    unitController.DestroyUnit();
                }
            }
        }
        
        private void SetRayCastOn() => _isRayCastOn = true;
        private void SetCollisionOn() => _isCollisionOn = true;

        #if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Vector3 targetPos = rayStartPos.position + (transform.forward * rayLength);
            Gizmos.DrawLine(rayStartPos.position,targetPos);
            Gizmos.DrawWireSphere(targetPos,sphereRayRadius);
        }
        #endif
        
        #endregion

        #region PUBLIC METHODS

        // At start get random direction and set it.
        public void StartAndSetInteractionController()
        {
            var randVector2 =  Random.insideUnitCircle;
            transform.forward = new Vector3(randVector2.x, 0, randVector2.y);
            _isInteractionActive = true;
        }

        public void StopUnitInteraction() => _isInteractionActive = false;

        #endregion


    }
}