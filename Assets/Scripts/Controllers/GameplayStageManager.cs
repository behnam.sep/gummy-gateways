using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Gameplay;
using Levels;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Controllers
{
    [DefaultExecutionOrder(-1000)]
    public class GameplayStageManager : MonoBehaviour
    {

        public static GameplayStageManager Instance { get; private set; }        
        
        #region ACTIONS
        public Action OnStageSettled;
        /// <summary>
        /// first int : next stage remaining player units,
        /// second int : stage level,
        /// GameArena value : spawned game arena for this stage
        /// Level : current level information
        /// </summary>
        public Action<int,int,GameArena,Level> OnStageArenaSpawn;
        public Action OnStageEnds;

        #endregion
        
        #region SERIALIZED PRIVATE FIELDS

        [SerializeField] private PlayerGroupController _playerGroupController;
        [SerializeField] private Transform cameraTransform;
        [SerializeField] private GameArena[] gameArenas;
        [SerializeField] private Transform arenaSpawnPosition;
        [SerializeField] private float arenaDistance;
        [SerializeField] private float camMovementDelay = 2;
        [Header("UI")] 
        [SerializeField] private GameObject unitCountsParent;
        [SerializeField] private GameObject stageDoneText;
        
        #endregion

        #region PRIVATE FIELDS

        private GameArena _lastSpawnedArena;
        private GameArena _currentSpawnedArena;
        private Level _currentLevel;
        private int _maxStageLevel;
        private int _currentStageLevel;
        private bool _gameWinCallActivated;
        private bool _gameLoseCallActivated;
        private bool _isTimerStarted;
        
        #endregion
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            LevelManager.OnLevelLoad += OnLevelLoadMethod;
            LevelManager.OnLevelStart += OnLevelStartMethod;
        }

        private void OnDestroy()
        {
            LevelManager.OnLevelStart -= OnLevelStartMethod;
            LevelManager.OnLevelLoad -= OnLevelLoadMethod;
        }

        #region PUBLIC METHODS
        
        public void GameWinCall()
        {
            _gameWinCallActivated = true;
            if (_isTimerStarted) return;
            _isTimerStarted = true;
            Invoke(nameof(EvaluateStageEndScenario),.05f);

        }

        public void GameLoseCall()
        {
            _gameLoseCallActivated = true;
            if (_isTimerStarted) return;
            _isTimerStarted = true;
            Invoke(nameof(EvaluateStageEndScenario),.05f);
        }

        #endregion

        #region PRIVATE METHODS
        
        private void AdvanceNextStage()
        {
            if (_currentStageLevel >= _maxStageLevel)
            {
                LevelManager.Instance.LevelComplete();
                return;
            }
            
            
            stageDoneText.SetActive(true);
            unitCountsParent.SetActive(false);
            
            
            
            // Set colliders settings for arena.
            _currentSpawnedArena.DisableWallColliders();
            _currentSpawnedArena.DisableUnitDestroyerColliders();
            _currentSpawnedArena.EnableNavmeshObstacles();
            _lastSpawnedArena = _currentSpawnedArena;
            _currentSpawnedArena.BackGateObstacle.enabled = true;
            _currentSpawnedArena.OpenDoorAnimation(
                () =>
                {
                    // Needs to movement of new units. When all units settled stage will be settled.
                    _playerGroupController.MoveUnitsToNextArena(() =>
                    {
                        // After all units settled, set colliders settings and invoke method.
                        Destroy(_lastSpawnedArena.gameObject);
                        StageSettledSettings();
                    });
                    
                }
            );
            
            var nextStagePlayerUnitCount = _playerGroupController.GetNextStageUnitCount();
            SpawnNewStageGameArena(nextStagePlayerUnitCount);
            // Start movement of camera.
            Invoke(nameof(StartCamMovementToNextStage),camMovementDelay);
            
        }

        private void SpawnNewStageGameArena(int nextStagePlayerUnits)
        {
            _currentStageLevel++;
            GameArena selectedArenaPrefab;
            if (_currentSpawnedArena != null)
            {
                // Make sure the new spawned arena not same with the last one.
                List<GameArena> adjustedList = gameArenas.Where(g => g.ArenaId != _currentSpawnedArena.ArenaId).ToList();
                var randIndex = Random.Range(0, adjustedList.Count);
                selectedArenaPrefab = adjustedList[randIndex];
            }

            else
            {
                var randIndex = Random.Range(0, gameArenas.Length);
                selectedArenaPrefab = gameArenas[randIndex];
            }

            var targetPos = arenaSpawnPosition.position + (Vector3.forward* ((_currentStageLevel-1) * arenaDistance));
            _currentSpawnedArena = Instantiate(selectedArenaPrefab, targetPos,selectedArenaPrefab.transform.rotation);
            _currentSpawnedArena.DisableWallColliders();
            _currentSpawnedArena.DisableUnitDestroyerColliders();
            _currentSpawnedArena.EnableNavmeshObstacles();
            
            _currentSpawnedArena.BackGateObstacle.enabled = false;
            // Door visual settings
            if (_currentStageLevel == 1)
            {
                // If first door backdoor not will be active
                _currentSpawnedArena.BackGate.SetActive(false);
                _currentSpawnedArena.FrontGate.SetActive(true);
            }
            else if (_currentStageLevel == _maxStageLevel)
            {
                // If it's last one front door will not be active
                _currentSpawnedArena.BackGate.SetActive(true);
                _currentSpawnedArena.FrontGate.SetActive(false);
            }
            else
            {
                // Else both active.
                _currentSpawnedArena.BackGate.SetActive(true);
                _currentSpawnedArena.FrontGate.SetActive(true);
            }
            
            OnStageArenaSpawn?.Invoke(nextStagePlayerUnits,_currentStageLevel,_currentSpawnedArena,_currentLevel);
        }

        private void StartCamMovementToNextStage()
        {
            var targetPos = cameraTransform.position + (Vector3.forward * arenaDistance);
            cameraTransform.DOMove(targetPos, 2f).SetEase(Ease.Linear);
        }
        
        private void EvaluateStageEndScenario()
        {
            _isTimerStarted = false;
            OnStageEnds?.Invoke();
            // This state for both all players and enemies units all destroyed, fail case for player.
            if (_gameWinCallActivated && _gameLoseCallActivated)
            {
                LevelManager.Instance.LevelFail();
                return;
            }

            if (_gameWinCallActivated)
            {
                _gameWinCallActivated = false;
                AdvanceNextStage();
                return;
            }

            if (_gameLoseCallActivated)
            {
                LevelManager.Instance.LevelFail();
            }
        }

        private void StageSettledSettings()
        {
            _currentSpawnedArena.DisableNavmeshObstacles();
            _currentSpawnedArena.EnableWallColliders();
            _currentSpawnedArena.EnableUnitDestroyerColliders();
            unitCountsParent.SetActive(true);
            stageDoneText.SetActive(false);
            
            _gameWinCallActivated = false;
            _isTimerStarted = false;
            
            OnStageSettled?.Invoke();
        }

        private void OnLevelStartMethod(Level level)
        {
            _maxStageLevel = level.StageCount;
            StageSettledSettings();
        }

        private void OnLevelLoadMethod(Level level)
        {
            _currentLevel = level;
            SpawnNewStageGameArena(0);
        }

        #endregion
    }
}