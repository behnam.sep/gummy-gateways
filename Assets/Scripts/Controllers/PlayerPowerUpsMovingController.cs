using System;
using Gameplay;
using Gameplay.PowerUps;
using Levels;
using UnityEngine;

namespace Controllers
{
    public class PlayerPowerUpsMovingController : MonoBehaviour
    {
        #region SERIALIZED PRIVATE FIELDS

        [SerializeField] private Camera camera;
        [SerializeField] private LayerMask slotPowerUpsLayer;
        [SerializeField] private LayerMask groundLayers;
        
        #endregion

        
        #region PRIVATE FIELDS

        private bool _isControllingOn = true;
        private bool _isControllingArea;
        private SlotPowerUpsControl _currentControllingPowerUp;
        
        #endregion
        
        #region UNITY METHODS

        private void Start()
        {
            GameplayStageManager.Instance.OnStageSettled += OnStageSettledMethod;
            GameplayStageManager.Instance.OnStageEnds += OnStageEndsMethod;
        }

        private void Update()
        {
            if (!_isControllingOn) return;

            bool isInPlayableGround = false;
            
            if (_isControllingArea)
            {
                var ray = camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hitInfo,10000, groundLayers))
                {
                    if (hitInfo.collider.CompareTag("PlayableGround"))
                    {
                        isInPlayableGround = true;
                        var hitPos = hitInfo.point;
                        // Only update x and z position of area.
                        var areaTransform = _currentControllingPowerUp.transform;
                        var targetPos = new Vector3(hitPos.x, areaTransform.position.y, hitPos.z);
                        areaTransform.position = targetPos;
                    }

                    // Else it's not playable ground
                    else
                    {
                        var hitPos = hitInfo.point;
                        // Only update x and z position of area.
                        var areaTransform = _currentControllingPowerUp.transform;
                        var targetPos = new Vector3(hitPos.x, areaTransform.position.y, hitPos.z);
                        areaTransform.position = targetPos;
                    }
                }
            }
            
            if (Input.GetMouseButtonDown(0) && !_isControllingArea)
            {
                var ray = camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hitInfo,10000, slotPowerUpsLayer))
                {
                    var powerUp = hitInfo.collider.GetComponent<SlotPowerUpsControl>();
                    powerUp.TryAndStartControlling(out bool isControllable);
                    if (isControllable)
                    {
                        _currentControllingPowerUp = powerUp;
                        _isControllingArea = true;
                    }
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (_isControllingArea)
                {
                    _isControllingArea = false;
                    _currentControllingPowerUp.StopControlling(isInPlayableGround);
                    _currentControllingPowerUp = null;
                }
            }
        }


        private void OnDestroy()
        {
            GameplayStageManager.Instance.OnStageSettled -= OnStageSettledMethod;
            GameplayStageManager.Instance.OnStageEnds -= OnStageEndsMethod;
        }

        #endregion

        #region PRIVATE METHODS

        private void OnStageEndsMethod()
        {
            _isControllingOn = false;
            _isControllingArea = false;
            if (_currentControllingPowerUp != null)
            {
                Destroy(_currentControllingPowerUp.gameObject);
            }
        }

        private void OnStageSettledMethod()
        {
            _isControllingOn = true;
        }
        
        

        #endregion
       
    }
}