using System;
using Gameplay;
using Levels;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Controllers
{
    public class PlayerGroupController : UnitsGroupController
    {
        
        #region PRIVATE FIELDS

        private Action _onAllUnitsSettled;
        private int _totalMovingUnitsCount;
        private int _currentSettledUnitsCount;

        #endregion
        
        #region OVERRIDE ABSTRACT METHODS
        
        protected override void AllUnitsDestroyed()
        {
            // Game fail.
            //Debug.Log("Player's all units destroyed, game fail.");
            GameplayStageManager.Instance.GameLoseCall();
        }
        
        protected override void UpdateCountUI()
        {
            UiController.Instance.UpdatePlayerUnitCount(_activeUnits.Count);
        }
        
        protected override void OnStageArenaSpawnMethod(int nextStagePlayerUnitCount,int stageLevel, GameArena arenaSpawned,Level level)
        {
            _arenaSpawnPositions = arenaSpawned.PlayerSpawnPoints;
            if (stageLevel == 1)
            {
                startUnitCount = level.PlayerStartSpawnCount;
                _currentMaxSpawnCount = baseMaxSpawnCount + (int)((level.levelNumber) * levelIncSpawnCount);
                SpawnUnitsAtSpawnPos(startUnitCount);
            }
        }

        #endregion

        #region PUBLIC METHODS

        public void MoveUnitsToNextArena(Action OnUnitsSettled)
        {
            _currentSettledUnitsCount = 0;
            _totalMovingUnitsCount = 0;
            _onAllUnitsSettled = OnUnitsSettled;
            Invoke(nameof(StarUnitsMovement),.25f);
        }

        public int GetNextStageUnitCount() => Mathf.Min(_activeUnits.Count, _arenaSpawnPositions.Length);

        #endregion

        #region PRIVATE METHODS

        private void UpdateSettledUnitCount()
        {
            _currentSettledUnitsCount++;
            if (_currentSettledUnitsCount >= _totalMovingUnitsCount)
            {
                _onAllUnitsSettled?.Invoke();
            }
        }

        private void StarUnitsMovement()
        {
            for (int i = 0; i < _arenaSpawnPositions.Length; i++)
            {
                if (i >= _activeUnits.Count)
                    break;

                _totalMovingUnitsCount++;
                var target = _arenaSpawnPositions[i];
                var unit = _activeUnits[i];
                unit.UnitMover.StartNavMeshMovement(target,UpdateSettledUnitCount);
            }
        }

        #endregion
    }
}