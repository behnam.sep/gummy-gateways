using Gameplay;
using Levels;
using UnityEngine;

namespace Controllers
{
    public class EnemyGroupController : UnitsGroupController
    {
        [SerializeField] private int minSpawnExtraValue = 3;
        [SerializeField] private int maxSpawnExtraValue = 10;

        // This method is called when all units in the group are destroyed.
        protected override void AllUnitsDestroyed()
        {
            GameplayStageManager.Instance.GameWinCall();
        }

        // This method updates the UI with the current count of active units.
        protected override void UpdateCountUI()
        {
            UiController.Instance.UpdateEnemyUnitCount(_activeUnits.Count);
        }

        // This method handles the spawning of enemy units based on the stage and level.
        protected override void OnStageArenaSpawnMethod(int nextStagePlayerUnitCount, int stageLevel, GameArena arenaSpawned, Level level)
        {
            _arenaSpawnPositions = arenaSpawned.EnemySpawnPoints;
            startUnitCount = level.EnemyStartSpawnCount;
            _currentMaxSpawnCount = baseMaxSpawnCount + (int)(level.levelNumber * levelIncSpawnCount);

            int spawnCount;

            if (stageLevel == 1)
            {
                spawnCount = startUnitCount;
            }
            else
            {
                spawnCount = nextStagePlayerUnitCount + Random.Range(minSpawnExtraValue, maxSpawnExtraValue + 1);
                spawnCount = Mathf.Min(spawnCount, _arenaSpawnPositions.Length);
            }

            SpawnUnitsAtSpawnPos(spawnCount);
        }

        // Add any additional methods or logic modifications below as needed.
    }
}
